import re
from abc import ABC, abstractmethod
from sys import argv as arguments


class ArgumentHandler:
    class InvalidNumberOfArgumentsException(Exception):
        def __init__(self, message: str) -> None:
            self.message = message

    def __init__(self, arguments: list) -> None:
        self.__arguments = arguments
        if not self.__arguments_are_valid():
            raise ArgumentHandler.InvalidNumberOfArgumentsException(
                "There must be one argument indicating filename for the program. Entered: {}.".format(len(self.__arguments) - 1))

    def __arguments_are_valid(self) -> bool:
        return len(self.__arguments) == 2

    @property
    def input_filename(self) -> str:
        return self.__arguments[1]


class InputFileHandler:

    def __init__(self, filename: str) -> None:
        with open(filename, "r", encoding="utf-8") as input_file:
            self.__raw_content = input_file.read()

    @property
    def commands(self):
        return self.__raw_content.split("\n")


class CommandHandler:

    def __init__(self, commands: list) -> None:
        self.commands = commands

    @staticmethod
    def operation_dict(args: str) -> dict:
        """Returns a dictionary containing command objects for specified arguments."""
        return {
            Command.CREATE_HALL: CreateHall(args),
            Command.SELL_TICKET: SellTicket(args),
            Command.CANCEL_TICKET: CancelTicket(args),
            Command.BALANCE: Balance(args),
            Command.SHOW_HALL: ShowHall(args)
        }

    def execute_all(self) -> None:
        for command in self.commands:
            try:
                args = Command.get_command_arg_as_str(command)
                CommandHandler.operation_dict(args)[Command.get_command_base(command).upper()].execute()
            except (KeyError, IndexError):
                if len(command) != 0:
                    Output.print("The command \'{}\' is invalid.".format(command), prefix=Output.Prefix.ERROR)
            except (Command.InvalidCommandException, Command.BadArgumentException) as e:
                Output.print(e.message, prefix=Output.Prefix.ERROR)
            except Command.IllegalOperationException as iae2:
                Output.print(iae2.message, prefix=Output.Prefix.WARNING)


class Command:
    CREATE_HALL = "CREATEHALL"
    SELL_TICKET = "SELLTICKET"
    CANCEL_TICKET = "CANCELTICKET"
    BALANCE = "BALANCE"
    SHOW_HALL = "SHOWHALL"

    class InvalidCommandException(Exception):
        def __init__(self, message: str) -> None:
            self.message = message

    class BadArgumentException(Exception):
        def __init__(self, message: str) -> None:
            self.message = message

    class IllegalOperationException(Exception):
        def __init__(self, message: str) -> None:
            self.message = message

    @staticmethod
    def get_command_base(command: str) -> str:
        return command.split()[0]

    @staticmethod
    def get_command_arg_as_str(command: str) -> str:
        return " ".join(command.split()[1:])


class HallException:
    class HallDoesNotExistException(Command.IllegalOperationException):
        pass

    class HallColumnOutOfRangeException(Command.BadArgumentException):
        pass

    class HallRowOutOfRangeException(Command.BadArgumentException):
        pass

    class SeatAlreadyOccupiedException(Command.IllegalOperationException):
        pass

    class HallAlreadyCreatedException(Command.IllegalOperationException):
        pass


class BaseCommand(ABC):
    """An abstract class for all command objects."""

    @abstractmethod
    def execute(self):
        pass


class CreateHall(BaseCommand):
    REGEX = r"[^ ]+ \d+x\d+"

    def __init__(self, args: str) -> None:
        self.args = args

    def check_validity(self) -> None:
        arg_list = self.args.split()
        if re.fullmatch(CreateHall.REGEX, self.args) is not None:
            hall_name_arg = arg_list[0]
            hall_size_arg = arg_list[1]
            rows = int(hall_size_arg.split("x")[0])
            columns = int(hall_size_arg.split("x")[1])

            if rows > 26:
                raise Command.BadArgumentException("Number of rows cannot be greater than 26. Entered: {}.".format(int(rows)))

            if rows == 0:
                raise Command.BadArgumentException("Number of rows cannot be 0.")

            if columns == 0:
                raise Command.BadArgumentException("Number of columns cannot be 0.")

            for hall in Cinema.halls.values():
                if hall.name == hall_name_arg:
                    raise HallException.HallAlreadyCreatedException("Cannot create the hall \'{0}\' for a second time. Cinema already has {0}.".format(hall.name))
        else:
            if len(arg_list) == 2:
                hall_size_arg = arg_list[1]
                if "x" in hall_size_arg:
                    hall_size_arg_list = hall_size_arg.split("x")
                    if len(hall_size_arg_list) > 2:
                        raise Command.BadArgumentException("Cannot create a hall that has three or more dimensions.")
                    rows = hall_size_arg_list[0]
                    columns = hall_size_arg_list[1]
                    if not rows.isdigit():
                        raise Command.BadArgumentException("Row size \'{}\' must be a positive integer.".format(rows))
                    if not columns.isdigit():
                        raise Command.BadArgumentException("Column size \'{}\' must be a positive integer.".format(columns))
                else:
                    raise Command.BadArgumentException("Second argument must contain \'x\' character in order to construct hall plan.")
            else:
                raise Command.BadArgumentException("Number of arguments for CREATEHALL must be 2, which is {} {} than current one.".format(
                    abs(2 - len(arg_list)), "less" if len(arg_list) > 2 else "more"))

    def get_args(self) -> tuple:
        self.check_validity()
        return self.args.split()[0], self.args.split()[1]

    def execute(self) -> None:
        hall_name_arg, hall_size_arg = self.get_args()
        hall = Hall(hall_name_arg, int(hall_size_arg.split("x")[0]), int(hall_size_arg.split("x")[1]))
        Cinema.halls.update({hall.name: hall})  # Adds a new hall object into the dictionary.
        Output.print("The hall \'{}\' having {} seat{} have been successfully created.".format(hall.name, hall.hall_size, "s" if hall.hall_size > 1 else ""),
                     prefix=Output.Prefix.SUCCESS)


class SellTicket(BaseCommand):
    REGEX = r"[^ ]+ (full|student) [^ ]+ .+"

    SEAT_NAME_REGEX = r"(([a-z]|[A-Z])(\d+))|((([a-z]|[A-Z])(\d+)-(\d+)))"

    def __init__(self, args: str) -> None:
        self.args = args

    @property
    def customer_name(self):
        return self.args.split()[0]

    @property
    def customer_fare(self):
        return self.args.split()[1]

    @property
    def hall_name(self):
        return self.args.split()[2]

    @property
    def seat_arguments(self):
        return self.args.split()[3:]

    def sell(self, seat_arg: str) -> None:
        customer = Customer(self.customer_name, self.customer_fare)
        if Seat.is_seat_range(seat_arg):
            for absolute_seat in SellTicket.get_absolute_seats(seat_arg):
                position = (Util.get_letter_index(absolute_seat[0]), int(absolute_seat[1:]))
                Cinema.halls[self.hall_name].add_customer(position, customer)
        else:
            position = (Util.get_letter_index(seat_arg[0]), int(seat_arg[1:]))
            Cinema.halls[self.hall_name].add_customer(position, customer)
        Output.print("{} has bought {} at {}.".format(self.customer_name, seat_arg, self.hall_name), prefix=Output.Prefix.SUCCESS)

    def execute(self) -> None:
        self.check_command_validity()
        self.start_selling()

    def start_selling(self) -> None:
        """Treats every seat individually via try-except."""
        for seat_arg in self.seat_arguments:
            try:
                self.check_seat_arg_validity(seat_arg)
                self.sell(seat_arg)
            except (HallException.HallColumnOutOfRangeException, HallException.HallRowOutOfRangeException, Command.BadArgumentException) as e:
                Output.print(e.message, prefix=Output.Prefix.ERROR)
            except HallException.SeatAlreadyOccupiedException as sao:
                Output.print(sao.message, prefix=Output.Prefix.WARNING)

    def check_seat_arg_validity(self, seat_arg: str) -> None:
        if re.fullmatch(SellTicket.SEAT_NAME_REGEX, seat_arg) is None:
            raise Command.BadArgumentException("The seat argument \'{}\' is invalid.".format(seat_arg))

        seat_matrix = Cinema.halls[self.hall_name].seat_matrix

        row_index = Util.get_letter_index(seat_arg[0])

        if row_index >= Cinema.halls[self.hall_name].rows:
            raise HallException.HallRowOutOfRangeException(
                "The hall \'{}\' has less row than the specified row {}.".format(self.hall_name, seat_arg[0]))

        if Seat.is_seat_range(seat_arg):
            seat_column_slice = seat_arg[1:].split("-")
            seat_column_start = int(seat_column_slice[0])
            seat_column_end = int(seat_column_slice[1])

            if seat_column_start >= seat_column_end:
                raise Command.BadArgumentException(
                    "Cannot sell seats \'{}\', because first index of seat range cannot be greater than or equal to last index.".format(seat_arg))

            if seat_column_end >= len(seat_matrix[row_index]) + 1:
                raise HallException.HallColumnOutOfRangeException(
                    "The hall \'{}\' has less column than the specified index in {}.".format(self.hall_name, seat_arg))

            if False in [(seat.customer is None) or seat.customer.name == self.customer_name for seat in
                         [seat_matrix[row_index][c] for c in range(seat_column_start, seat_column_end)]]:
                raise HallException.SeatAlreadyOccupiedException(
                    "The seats {} at {} cannot be sold to {}, because some of them have already been sold.".format(seat_arg, self.hall_name, self.customer_name))
        else:
            column_index = int(seat_arg[1:])

            if column_index >= len(seat_matrix[row_index]):
                raise HallException.HallColumnOutOfRangeException(
                    "The hall \'{}\' has less column than the specified index in {}.".format(self.hall_name, seat_arg))

            seat = seat_matrix[row_index][column_index]
            if seat.customer is not None:
                raise HallException.SeatAlreadyOccupiedException(
                    "The seat {} at {} cannot be sold to {}, because they already bought it.".format(seat_arg, self.hall_name, self.customer_name)
                    if seat.customer.name == self.customer_name
                    else "The seat {} at {} cannot be sold to {} because it already been sold to {}.".format(seat_arg, self.hall_name, self.customer_name, seat.customer.name))

    def check_command_validity(self) -> None:
        arg_list = self.args.split()
        if re.fullmatch(SellTicket.REGEX, self.args) is not None:
            if self.hall_name not in Cinema.halls.keys():
                raise HallException.HallDoesNotExistException(
                    "Cannot sell ticket to {} because the hall {} does not exist.".format(self.customer_name, self.hall_name))
        else:
            if len(arg_list) < 4:
                raise Command.InvalidCommandException("The command SELLTICKET must have at least 4 arguments. Only {} entered.".format(len(arg_list)))
            if arg_list[1].lower() != Customer.FARE_STUDENT and arg_list[1].lower() != Customer.FARE_FULL:
                raise Command.BadArgumentException("Second argument of SELLTICKET must indicate fare: full or student.")
            raise Command.InvalidCommandException("The command \"SELLTICKET {}\" is invalid.".format(self.args))

    @staticmethod
    def get_absolute_seats(seat_arg: str) -> list:
        """
        Returns absolute seat list for provided seat range.
        For example, for seat range A3-6, returns ['A3', 'A4', 'A5'].
        """
        abs_seats = []
        if "-" in seat_arg:
            row_letter = seat_arg[0].upper()
            seat_column_slice = seat_arg[1:].split("-")
            seat_column_start = int(seat_column_slice[0])
            seat_column_end = int(seat_column_slice[1])
            abs_seats.extend([row_letter + str(column) for column in range(seat_column_start, seat_column_end)])
        else:
            abs_seats.append(seat_arg)
        return abs_seats


class CancelTicket(BaseCommand):
    REGEX = r"[^ ]+ .+"

    SEAT_NAME_REGEX = r"(([a-z]|[A-Z])(\d+))|((([a-z]|[A-Z])(\d+)-(\d+)))"

    def __init__(self, args: str) -> None:
        self.args = args

    def execute(self):
        self.check_command_validity()
        self.start_cancelling()

    def cancel(self, seat_arg: str) -> None:
        already_free = False
        seat_range_all_occupied = False
        seats_already_free = []
        is_seat_range = Seat.is_seat_range(seat_arg)
        absolute_seats = SellTicket.get_absolute_seats(seat_arg)

        if is_seat_range:
            for absolute_seat in absolute_seats:
                position = (Util.get_letter_index(absolute_seat[0]), int(absolute_seat[1:]))
                if self.hall.get_seat(position).customer is None:
                    seats_already_free.append(absolute_seat)
            if len(seats_already_free) == len(absolute_seats):
                already_free = True
            if len(seats_already_free) == 0:
                seat_range_all_occupied = True
        else:
            position = (Util.get_letter_index(seat_arg[0]), int(seat_arg[1:]))
            if self.hall.get_seat(position).customer is None:
                already_free = True

        if not already_free:
            if is_seat_range:
                if seat_range_all_occupied:
                    for absolute_seat in absolute_seats:
                        position = (Util.get_letter_index(absolute_seat[0]), int(absolute_seat[1:]))
                        self.hall.remove_customer(position)
                    Output.print("The ticket for seat range {} at {} has been cancelled.".format(seat_arg, self.hall_name), prefix=Output.Prefix.SUCCESS)
                else:
                    for absolute_seat in absolute_seats:
                        position = (Util.get_letter_index(absolute_seat[0]), int(absolute_seat[1:]))
                        if self.hall.get_seat(position).customer is None:
                            Output.print("The seat {} at {} is already free. Nothing to cancel.".format(absolute_seat, self.hall_name), prefix=Output.Prefix.ERROR)
                        else:
                            self.hall.remove_customer(position)
                            Output.print("The ticket for seat {} at {} has been cancelled.".format(absolute_seat, self.hall_name), prefix=Output.Prefix.SUCCESS)
            else:
                position = (Util.get_letter_index(seat_arg[0]), int(seat_arg[1:]))
                self.hall.remove_customer(position)
                Output.print("The ticket for seat {} at {} has been cancelled.".format(seat_arg, self.hall_name), prefix=Output.Prefix.SUCCESS)
        else:
            message_already_free = "The seat{} {} at {} is already free. Nothing to cancel.".format("s" if is_seat_range else "", seat_arg, self.hall_name)
            Output.print(message_already_free, prefix=Output.Prefix.SUCCESS)

    def start_cancelling(self) -> None:
        for seat_arg in self.seat_arguments:
            try:
                self.check_seat_arg_validity(seat_arg)
                self.cancel(seat_arg)
            except (HallException.HallColumnOutOfRangeException, HallException.HallRowOutOfRangeException, Command.BadArgumentException) as e:
                Output.print(e.message, prefix=Output.Prefix.ERROR)

    def check_seat_arg_validity(self, seat_arg: str) -> None:
        if re.fullmatch(CancelTicket.SEAT_NAME_REGEX, seat_arg) is None:
            raise Command.BadArgumentException("The seat argument \'{}\' is invalid.".format(seat_arg))

        seat_matrix = Cinema.halls[self.hall_name].seat_matrix

        row_index = Util.get_letter_index(seat_arg[0])

        if row_index >= Cinema.halls[self.hall_name].rows:
            raise HallException.HallRowOutOfRangeException(
                "The hall \'{}\' has less row than the specified row {}.".format(self.hall_name, seat_arg[0]))

        if Seat.is_seat_range(seat_arg):
            seat_column_slice = seat_arg[1:].split("-")
            seat_column_start = int(seat_column_slice[0])
            seat_column_end = int(seat_column_slice[1])
            if seat_column_start >= seat_column_end:
                raise Command.BadArgumentException(
                    "Cannot cancel tickets for seats \'{}\', because first index of seat range cannot be greater than or equal to last index.".format(seat_arg))

            if seat_column_end >= len(seat_matrix[row_index]) + 1:
                raise HallException.HallColumnOutOfRangeException(
                    "The hall \'{}\' has less column than the specified index in {}.".format(self.hall_name, seat_arg))
        else:
            column_index = int(seat_arg[1:])
            if column_index >= len(seat_matrix[row_index]):
                raise HallException.HallColumnOutOfRangeException(
                    "The hall \'{}\' has less column than the specified index in {}.".format(self.hall_name, seat_arg))

    @property
    def hall_name(self):
        return self.args.split()[0]

    @property
    def hall(self):
        return Cinema.halls[self.hall_name]

    @property
    def seat_arguments(self):
        return self.args.split()[1:]

    def check_command_validity(self):
        arg_list = self.args.split()
        if re.fullmatch(CancelTicket.REGEX, self.args) is not None:
            hall_name = arg_list[0]
            seat_args = arg_list[1:]

            if hall_name not in Cinema.halls.keys():
                raise HallException.HallDoesNotExistException(
                    "Cannot cancel ticket{} because the hall {} does not exist".format("s" if len(seat_args) > 1 else "", hall_name))
        else:
            if len(arg_list) < 2:
                raise Command.InvalidCommandException("The command CANCELTICKET must have at least 2 arguments. Only {} entered.".format(len(arg_list)))
            raise Command.InvalidCommandException("The command \"CANCELTICKET {}\" is invalid.".format(self.args))


class Balance(BaseCommand):
    PRICE_STUDENT = 5
    PRICE_FULL = 10

    def __init__(self, args: str) -> None:
        self.args = args

    def execute(self):
        if len(self.hall_names) < 1:
            raise Command.InvalidCommandException("The command BALANCE must include at least one argument.")
        for name in self.hall_names:
            if name in Cinema.halls.keys():
                title = "Hall report of \'{}\'".format(name)
                Output.print(title, prefix=Output.Prefix.SUCCESS)
                Output.print("-" * len(title), prefix=" " * Output.Prefix.PADDING)
                Output.print(Cinema.halls[name].balance)
            else:
                Output.print("Cannot show hall report of \'{}\' because the hall does not exist.".format(name), prefix=Output.Prefix.ERROR)

    @property
    def hall_names(self):
        return self.args.split()


class ShowHall(BaseCommand):

    def __init__(self, args: str) -> None:
        self.args = args

    def execute(self):
        if len(self.hall_names) < 1:
            raise Command.InvalidCommandException("The command SHOWHALL must include at least one argument.")
        for name in self.hall_names:
            if name in Cinema.halls.keys():
                Output.print("Showing hall \'{}\' below:".format(name), prefix=Output.Prefix.SUCCESS)
                Output.print(str(Cinema.halls[name]))
            else:
                Output.print("Cannot show hall \'{}\' because it does not exist.".format(name), prefix=Output.Prefix.ERROR)

    @property
    def hall_names(self):
        return self.args.split()


class Customer:
    FARE_FULL = "full"
    FARE_STUDENT = "student"

    def __init__(self, name: str, fare: str) -> None:
        self.name = name
        self.fare = fare


class Seat:
    SEAT_EMPTY = "X"  # Personal note: This would have been much kinder to the eyes if this was simply "."
    SEAT_STUDENT = "S"
    SEAT_FULL = "F"

    def __init__(self, customer) -> None:
        self.customer = customer

    def __str__(self):
        if self.customer is None:
            return Seat.SEAT_EMPTY
        if self.customer.fare == "student":
            return Seat.SEAT_STUDENT
        if self.customer.fare == "full":
            return Seat.SEAT_FULL

    @staticmethod
    def is_seat_range(seat_arg: str) -> bool:
        return "-" in seat_arg


class Hall:

    def __init__(self, name: str, rows: int, columns: int) -> None:
        self.name = name
        self.rows = rows
        self.columns = columns
        self.seat_matrix = [[Seat(None) for _ in range(self.columns)] for _ in range(self.rows)]

    def __str__(self):
        string = ""
        padding = len(str(self.columns - 1)) + 1  # Dynamically adds padding according to number of columns.
        padding_from_start = 3  # Output.Prefix.PADDING + 1
        for row in range(self.rows, -1, -1):  # Starts from the last, putting row A to the bottom.
            string += " " * (padding_from_start - 1)
            if row != self.rows:
                string += Util.LETTERS[row]
            for column in range(self.columns + 1):
                if row != self.rows and column != self.columns:
                    string += str(self.seat_matrix[row][column]).rjust(padding if column != 0 else 2)
            string += "\n"
        string += " " * padding_from_start + " 0" + "".join([str(i).rjust(padding) for i in range(1, self.columns)])
        string += "\n"
        return string

    @property
    def balance(self) -> str:
        balance = " " * Output.Prefix.PADDING
        revenue_student = self.number_of_students * Balance.PRICE_STUDENT
        revenue_full = self.number_of_full * Balance.PRICE_FULL
        balance += "Revenue from students: {} liras, ".format(revenue_student)
        balance += "Revenue from full fares: {} liras, ".format(revenue_full)
        balance += "Overall revenue: {} liras".format(revenue_full + revenue_student)
        return balance

    @property
    def hall_size(self) -> int:
        return self.rows * self.columns

    @property
    def number_of_students(self) -> int:
        n = 0
        for row_index in range(len(self.seat_matrix)):
            for column_index in range(len(self.seat_matrix[row_index])):
                seat = self.seat_matrix[row_index][column_index]
                if seat.customer is not None and seat.customer.fare == Customer.FARE_STUDENT:
                    n += 1
        return n

    @property
    def number_of_full(self) -> int:
        n = 0
        for row_index in range(len(self.seat_matrix)):
            for column_index in range(len(self.seat_matrix[row_index])):
                seat = self.seat_matrix[row_index][column_index]
                if seat.customer is not None and seat.customer.fare == Customer.FARE_FULL:
                    n += 1
        return n

    def add_customer(self, position: tuple, customer: Customer) -> None:
        self.seat_matrix[position[0]][position[1]] = Seat(customer)

    def remove_customer(self, position: tuple) -> None:
        self.seat_matrix[position[0]][position[1]] = Seat(None)

    def get_seat(self, position: tuple) -> Seat:
        return self.seat_matrix[position[0]][position[1]]


class Cinema:
    halls = dict()  # A dictionary that holds all hall objects.


class Util:
    LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    @staticmethod
    def get_letter_index(letter) -> int:
        return Util.LETTERS.index(letter.upper())


class Output:
    output_file = open("out.txt", "w", encoding="utf-8")

    class Prefix:  # Respecting example output format.
        PADDING = 9  # 10
        ERROR = "Error:".ljust(PADDING)  # "[ERROR]"
        WARNING = "Warning:".ljust(PADDING)  # "[WARNING]"
        SUCCESS = "Success:".ljust(PADDING)  # "[SUCCESS]"

    @staticmethod
    def print(value: object, prefix: str = None) -> None:
        print(str(value) if prefix is None else prefix + str(value))
        print(str(value) if prefix is None else prefix + str(value), file=Output.output_file)


def main() -> None:
    argument_handler = ArgumentHandler(arguments)
    input_file_handler = InputFileHandler(argument_handler.input_filename)
    command_handler = CommandHandler(input_file_handler.commands)
    command_handler.execute_all()


if __name__ != "__main__":
    Output.print("You are trying to run this program by importing as a module.", prefix=Output.Prefix.WARNING)

try:
    main()
except ArgumentHandler.InvalidNumberOfArgumentsException as iae:
    Output.print(iae.message, prefix=Output.Prefix.ERROR)
except FileNotFoundError as fnf:
    Output.print(fnf.strerror + ": {}".format(fnf.filename), prefix=Output.Prefix.ERROR)
finally:
    Output.output_file.close()
